package com.example.backend_thymeleaf.service.impl;

import com.example.backend_thymeleaf.controller.converter.CourseConverter;
import com.example.backend_thymeleaf.entity.Course;
import com.example.backend_thymeleaf.model.CourseModel;
import com.example.backend_thymeleaf.repository.CourseRepository;
import com.example.backend_thymeleaf.service.CourseService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {

    private static final Log LOG = LogFactory.getLog(CourseServiceImpl.class);

    @Autowired
    @Qualifier("courseConverter")
    private CourseConverter courseConverter;

    @Autowired
    @Qualifier("courseRepository")
    private CourseRepository courseRepository;

    @Override
    public List<CourseModel> listAll() {
        LOG.info("Call METHOD:"+"listAll()");
        List<Course> courses = courseRepository.findAll();
        return courses.stream().map(course -> courseConverter.toModel(course)).collect(Collectors.toList());
    }

    @Override
    public Course addCourse(CourseModel course) {
        LOG.info("Call METHOD:"+"---addCourse()");
        return courseRepository.save(courseConverter.toEntity(course));
    }

    @Override
    public int delete(int id) {
        courseRepository.delete(id);
        return 0;
    }

    @Override
    public Course update(Course course) {
        return courseRepository.save(course);
    }
}
