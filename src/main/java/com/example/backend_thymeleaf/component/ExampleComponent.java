package com.example.backend_thymeleaf.component;


import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component("exampleComponent")
public class ExampleComponent {

    public static  final Log LOGGER = LogFactory.getLog(ExampleComponent.class);

    public void componentLogger(){

        LOGGER.info("EXAMPLE COMPONENT");

    }

}
