package com.example.backend_thymeleaf.controller;


import com.example.backend_thymeleaf.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/example_with_model")
public class ExampleWithModelController {


    @GetMapping("/return_string_model")
    public String returnModelComplex(Model model){
        model.addAttribute("person", new Person("Juan Manuel",1));
        return "exampleWithModel";
    }

    @GetMapping("/return_model_and_view")
    public ModelAndView returnModelAndViewComplexModel(){
        ModelAndView modelAndView = new ModelAndView("exampleWithModel");
        modelAndView.addObject("person",new Person("Maria",30));
        return modelAndView;
    }

}
