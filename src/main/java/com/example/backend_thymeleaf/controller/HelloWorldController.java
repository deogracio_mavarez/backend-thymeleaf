package com.example.backend_thymeleaf.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/example")
public class HelloWorldController {

    @GetMapping("/hola_mundo")
    public String helloWorld(){
        return "holaMundo";
    }

}
