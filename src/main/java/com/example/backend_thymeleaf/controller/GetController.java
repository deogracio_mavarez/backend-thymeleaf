package com.example.backend_thymeleaf.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/exampleGet")
public class GetController {

    private static final String DEFAULT_VIEW = "getexample";


    @GetMapping("/param2/{name}")
    public ModelAndView getUrlDate(@PathVariable(name = "name") String name){
        ModelAndView modelAndView = new ModelAndView(DEFAULT_VIEW);
        modelAndView.addObject("name_in_model",name);
        return modelAndView;
    }




}
