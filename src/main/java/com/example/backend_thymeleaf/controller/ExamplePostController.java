package com.example.backend_thymeleaf.controller;


import com.example.backend_thymeleaf.component.ExampleComponent;
import com.example.backend_thymeleaf.model.Person;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/example_post")
public class ExamplePostController {

    @Autowired
    @Qualifier("exampleComponent")
    private ExampleComponent exampleComponent;

    public static final String DEFAULT_VIEW = "example_post";

    private static final Log LOGGER = LogFactory.getLog(ExamplePostController.class);

    @GetMapping("/show_form")
    public String showForm(Model model){
        addAtribute(model);
        LOGGER.info("INFO TRACE");
        LOGGER.warn("WARNING TRACE");
        LOGGER.error("ERROR TRACE");
        LOGGER.debug( "DEBUG TRACE");
        exampleComponent.componentLogger();
        return DEFAULT_VIEW;
    }

    //metodo para generar error 500 y manejarlo con handler
    @GetMapping("/show_error")
    public String showViewError(Model model){
        addAtribute(model);
        return DEFAULT_VIEW;
    }

    @PostMapping("/result_post_example")
    public ModelAndView showResult(@Valid @ModelAttribute("person") Person person, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();

        if(bindingResult.hasErrors()){
            modelAndView.setViewName(DEFAULT_VIEW);
        }else {
            modelAndView.setViewName("result_post_example");
            modelAndView.addObject("person", person);
            LOGGER.info("METHOD:'SHOWRESUL'    --- PARAMS:'" + person + "'");
            LOGGER.info("TEMPLATE: 'result_post_example'     ---DATA:'" + person + "'");

        }
        return modelAndView;
    }

    private Model addAtribute(Model model){
        return model.addAttribute("person", new Person());

    }

}
