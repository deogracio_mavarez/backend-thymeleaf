package com.example.backend_thymeleaf.model;

public class CourseModel {

    private String name;
    private String description;
    private int price;
    private int hours;

    public CourseModel(){}

    public CourseModel(String name, String description, Integer price, Integer hours) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.hours = hours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }
}
