package com.example.backend_thymeleaf.repository;

import com.example.backend_thymeleaf.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("courseRepository")
public interface CourseRepository extends JpaRepository<Course, Serializable> {

    Course findByPrice(int price);

    Course findByPriceAndName(int price, String name);

    List<Course> findByName(String name);

    Course  findByNameOrPrice(String name, int price);

}
